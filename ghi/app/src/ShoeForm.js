import React, { useEffect, useState } from 'react'


function ShoeForm() {
    const [mfr, setMfr] = useState('')
    const [name, setName] = useState('')
    const [color, setColor] = useState('')
    const [picUrl, setPicUrl] = useState('')
    const [bin, setBin] = useState('')
    const [bins, setBins] = useState([])

    const mfrChange = (event) => {
        const value = event.target.value
        setMfr(value)
    }

    const nameChange = (event) => {
        const value = event.target.value
        setName(value)
    }

    const colorChange = (event) => {
        const value = event.target.value
        setColor(value)
    }

    const binChange = (event) => {
        const value = event.target.value
        setBin(value)
    }

    const picUrlChange = (event) => {
        const value = event.target.value
        setPicUrl(value)
    }

    const fetchData = async () => {
        const url = 'http://localhost:8100/api/bins/'

        const response = await fetch(url)

        if (response.ok) {
            const data = await response.json()
            setBins(data.bins)
        }
    }

    useEffect(() => {
        fetchData()
    }, [])




    const submitBtnHandler = async (event) => {
        event.preventDefault()
        // create empty JSON data
        const data = {}
        data.manufacturer = mfr;
        data.name = name
        data.color = color
        data.bin_number = bin
        data.pic_url = picUrl

        const shoesUrl = `http://localhost:8080/api/shoes/`;
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(shoesUrl, fetchConfig);
        console.log(response)
        if (response.ok) {
            const newShoe = await response.json();
            console.log(newShoe);

            setName('')
            setMfr('')
            setColor('')
            setBin('')
            setPicUrl('')
        }
    }




    return (
<>
<main>
<div className="container">
<div className="row">
<div className="offset-3 col-6">
<div className="shadow p-4 mt-4">
    <h1>Create a New Shoe!</h1>
    <form id="create-presentation-form" onSubmit={submitBtnHandler}>
        <div className="form-floating mb-3">
            <input value={name} onChange={nameChange} placeholder="Name" required type="text" name="name" id="name" className="form-control"></input>
            <label htmlFor="name">Model name</label>
        </div>
        <div className="form-floating mb-3">
            <input value={mfr} onChange={mfrChange} name="manufacturer" type="text" className="form-control" id="manufacturer" placeholder="brand"></input>
            <label htmlFor="brand">Brand Name</label>
        </div>
        <div className="form-floating mb-3">
            <input value={color} onChange={colorChange} placeholder="Color" type="text" name="color" id="color" className="form-control"></input>
            <label htmlFor="color">Color</label>
        </div>
        <div className="form-floating mb-3">
            <input value={picUrl} onChange={picUrlChange} placeholder="Picture Url" type="text" name="pic_url" id="pic_url" className="form-control"></input>
            <label htmlFor="picture_url">Picture Url</label>
        </div>
        <div className="mb-3">
        <select value={bin} onChange={binChange} required name="bin_number" id="bin_number" className="form-select">
        <option value="bin">Select Bin</option>
        {bins.map(bin => {
            return (
                <option key={bin.id} value={bin.href}>
                    {bin.id}
                </option>
            )
        })}
        </select>
        </div>
        <button className="btn btn-primary">Create</button>
    </form>
</div>
</div>
</div>
</div>
</main>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
    integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p"
    crossOrigin="anonymous">
</script>

</>
    )
}

export default ShoeForm