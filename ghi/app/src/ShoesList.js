import React from 'react'

function ShoesList(props) {

    if (props.shoes === undefined) {
        return null
    }

    return (
<>
<main>
<table className='table table-striped'>
<thead>
    <tr>
        <th>Brand</th>
        <th>Model Name</th>
        <th>Color</th>
        <th>Picture</th>
    </tr>
</thead>
<tbody>
    {props.shoes.map(shoes => {
        return (
            <tr key={shoes.id}>
                <td>{shoes.mfr}</td>
                <td>{shoes.name}</td>
                <td>{shoes.color}</td>
                <td><img src={ shoes.pic_url } alt='' height="100" width="100" /></td>
            </tr>
        )
    })}
</tbody>
</table>

<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
    integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p"
    crossOrigin="anonymous">
</script>
</main>
</>
    )
}


export default ShoesList