import React from "react";


function HatsList(props) {

    if (props.hats === undefined) {
        return null
    }

    return (
        <>
        <main>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Fabric</th>
                        <th>Style Name</th>
                        <th>Color</th>
                        <th>Picture</th>
                        <th>Location</th>
                        <th>Delete</th>
                    </tr>
                </thead>
                <tbody>
                    {props.hats.map(hats => {
                        return (
                            <tr key={ hats.id }>
                                <td>{ hats.fabric }</td>
                                <td>{ hats.style_name }</td>
                                <td>{ hats.color }</td>
                                <td>{ hats.picture_url }</td>
                                <td>{ hats.location }</td>
                            </tr>
                        )
                    })}
                </tbody>
            </table>
            <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
    integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p"
    crossOrigin="anonymous">
    </script>
    </main>
    </>

    )
}


export default HatsList;
