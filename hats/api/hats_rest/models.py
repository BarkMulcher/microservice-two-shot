from django.db import models
from django.urls import reverse


class LocationVO(models.Model):
    closet_name = models.CharField(max_length=100)
    # section_number = models.PositiveSmallIntegerField()
    # shelf_number = models.PositiveSmallIntegerField()
    href = models.CharField(max_length=300, unique=True, null=True)

    def __str__(self):
        return f"{self.closet_name}"


# Create your models here.
class Hat(models.Model):
    name = models.CharField(max_length=50)
    fabric = models.CharField(max_length=50)
    style_name = models.CharField(max_length=50)
    color = models.CharField(max_length=50)
    picture_url = models.URLField(null=True)
    location = models.ForeignKey(
        LocationVO,
        related_name="hats",
        on_delete=models.CASCADE,
        null=True,
        blank=True,
    )

    def get_api_url(self):
        return reverse("api_list_hats", kwargs={"pk": self.pk})

    def __str__(self):
        return self.name
