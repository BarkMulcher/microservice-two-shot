from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from .models import Hat, LocationVO
from common.json import ModelEncoder
import json


class HatListEncoder(ModelEncoder):
    model = Hat
    properties = ["style_name", "color"]


class LocationVOEncoder(ModelEncoder):
    model = LocationVO
    properties = [
        "closet_name",
        # "section_number",
        # "shelf_number",
        "href",
    ]


class HatDetailEncoder(ModelEncoder):
    model = Hat
    properties = [
        "name",
        "fabric",
        "style_name",
        "color",
        "picture_url",
        "location",
        ]
    encoders = {
        'location': LocationVOEncoder()
    }


@require_http_methods(["GET", "POST"])
def api_list_hats(request):
    if request.method == "GET":
        hats = Hat.objects.all()
        return JsonResponse(
            {"hats": hats},
            encoder=HatListEncoder,
        )
    else:
        content = json.loads(request.body)
        try:
            href = content["location"]
            location = LocationVO.objects.get(href=href)
            content["location"] = location
        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid Location"},
                status=400,
            )
        hat = Hat.objects.create(**content)
        return JsonResponse(
            hat,
            encoder=HatDetailEncoder,
            safe=False
        )


@require_http_methods(["GET", "DELETE", "PUT"])
def api_show_hat(request, pk):
    if request.method == "GET":
        hat = Hat.objects.get(id=pk)
        return JsonResponse(
            hat,
            encoder=HatDetailEncoder,
            safe=False
        )
    elif request.method == "PUT":
        content = json.loads(request.body)
        try:
            if "location" in content:
                current_location = LocationVO.objects.get(href=content["location"])
                content["location"] = current_location
        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid Location VO"},
                status=400,
            )
        Hat.objects.filter(id=pk).update(**content)
        hat = Hat.objects.get(pk=id)
        return JsonResponse(
            hat,
            encoder=HatDetailEncoder,
            safe=False,
        )







        # try:
        #     hat = Hat.objects.get(id=pk)
        #     hat.delete()
        #     return JsonResponse(
        #         hat,
        #         encoder=HatDetailEncoder,
        #         safe=False,
        #     )
        # except Hat.DoesNotExist:
        #     return JsonResponse(
        #         {"message": "Hat does not exist."},
        #         status=400,)
    # else:
    #     # content = json.loads(request.body)
    #     # COME BACK AND FIGURE OUT PUT ...
