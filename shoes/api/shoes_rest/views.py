from django.shortcuts import render
from django.http import JsonResponse, HttpResponse
from django.views.decorators.http import require_http_methods
import json
from common.json import ModelEncoder
from .models import Shoes, BinVO
from django.core import serializers

# shoe list encoder
class ShoeListEncoder(ModelEncoder):
    model = Shoes
    properties = [
        'manufacturer',
        'name',
        'color',
        'bin_number',
        'pic_url',
        'id',
    ]

    def get_extra_data(self, o):
        return {'bin_number': o.bin_number.id}

class BinVOEncoder(ModelEncoder):
    model = BinVO
    properties =[
        'href',
        # 'bin_number',
        # 'bin_size',
        'closet_name',
    ]

class ShoeDetailEncoder(ModelEncoder):
    model = Shoes
    properties = [
        'manufacturer',
        'name',
        'color',
        'pic_url',
        'bin_number',
        'id',
    ]
    encoders = {
        'bin_number': BinVOEncoder()
    }

# get list of shoes
@require_http_methods(['GET', 'POST'])
def api_list_shoes(request, bin_number_vo_id=None):
    if request.method == 'GET':
        if bin_number_vo_id==None:
            shoes = Shoes.objects.all()
        else:
            shoes = Shoes.objects.filter(bin_number=bin_number_vo_id)
        return JsonResponse(
            {'shoes': shoes},
            encoder=ShoeListEncoder,
        )
    else:
        content = json.loads(request.body)
        try:
            print(content)
            href = content['bin_number']
            print(href)
            bin_number = BinVO.objects.get(href=href)
            content['bin_number'] = bin_number
            shoe = Shoes.objects.create(**content)
            return JsonResponse(
                shoe,
                encoder=ShoeListEncoder,
                safe=False,
            )
        except BinVO.DoesNotExist:
            return JsonResponse(
                {'message': 'invalid bin'},
                status=400,
            )


# show shoe detail
@require_http_methods(['PUT', 'DELETE', 'GET'])
def api_shoe_detail(request, pk):
    if request.method == 'GET':
        shoe = Shoes.objects.get(id=pk)
        shoe_data = {
            'manufacturer': shoe.manufacturer,
            'name': shoe.name,
            'color': shoe.color,
            'pic_url': shoe.pic_url,
            # 'bin_number': shoe.bin_number,
            'id': shoe.id
        }
        return JsonResponse(
            shoe_data,
            encoder=ShoeDetailEncoder,
            safe=False,
        )

    elif request.method =='DELETE':
        count, _ = Shoes.objects.filter(id=pk).delete()
        return JsonResponse({'deleted': count > 0})
    else:
        content = json.loads(request.body)
        try:
            if 'bin' in content:
                bin = BinVO.objects.get(id=content['bin'])
                content['bin'] = bin
        except BinVO.DoesNotExist:
            return JsonResponse(
                {'messge': 'invalid bin number'},
                status=400,
            )
        Shoes.objects.filter(id=pk).update(**content)
        shoes = Shoes.objects.get(id=pk)
        return JsonResponse(
            shoes,
            encoder=ShoeDetailEncoder,
            safe=False,
        )
