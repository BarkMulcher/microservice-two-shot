from django.db import models
from django.urls import reverse

class BinVO(models.Model):
    # from wardrobe api app
    href = models.URLField(unique=True, max_length=200, null=True, blank=True)
    # bin_number = models.PositiveSmallIntegerField()
    # bin_size = models.PositiveSmallIntegerField()
    closet_name = models.CharField(max_length=200)

    def __str__(self):
        return f"{self.closet_name}"


class Shoes(models.Model):
    manufacturer = models.CharField(max_length=200)
    name = models.CharField(max_length=200)
    color = models.CharField(max_length=200)
    pic_url = models.URLField(null=True, max_length=200)

    bin_number = models.ForeignKey(
        BinVO,
        related_name='shoes',
        on_delete=models.CASCADE,
    )

    def get_api_url(self):
        return reverse("api_list_shoes", kwargs={"pk": self.pk})

    def __str__(self):
        return self.name



