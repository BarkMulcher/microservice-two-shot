# Wardrobify

Team:

<<<<<<< HEAD
* Luke - Which microservice?
Shoes
* MK - Which microservice?
Hats
=======
* Person 1 - Luke Haskell - Which microservice? - Shoes
* Person 2 - MK Grissom - Which microservice? - Hats
>>>>>>> 6199ad8526d45340db3896d5a7efd7522c5a713e

## Design

## Shoes microservice

Explain your models and integration with the wardrobe
microservice, here.

## Hats microservice
Explain your models and integration with the wardrobe
microservice, here.
1. Go to hats_project/settings.py and add hats app to INSTALLED_APPS

hats/api: (port: 8090)
1. Create a Hat model that tracks: X
    -fabric
    -style name
    -color
    -picture url
    -location (comes from a location VO)

2. Create views: X
    - List Hats (get, post)
    - Hat Detail (get, put, delete)

3. URLs: X
    - Add url paths in hats_rest
    - Add url include in hats_project

4. Add request methods to Insomnia and return empty list of Hats X


hats/poll:
1. Figure out how polling works..... X
2. Create a function and make sure you CALL IT IN THE POLL, MK (note: this step was added after Noah helped me fix it by actually calling the function) X

React:
1. Show a list of hats and their details
2. Form to create a new hat
3. Provide a way to delete a hat
4. Route existing nav links to your components
